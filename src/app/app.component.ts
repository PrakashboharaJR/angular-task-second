import { Component } from '@angular/core';
import { Service } from './service/service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  // Variable Declaration
  public cart: any = [];
  public count: number = 0;
  public isLoggedIn: boolean = false;

  // Constructor
  constructor(
    private _service: Service,
    public router: Router
  ) {
    this._service.initCart();
    this.getCart()

    this._service.cartChanged$
      .subscribe(cart => {
        this.getCart();
      });
  }

  ngOnInit(): void { }

  // Getting Cart on app loads
  getCart() {
    this.cart = this._service.getCart();
  }



}
