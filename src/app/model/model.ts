export interface ProductModel {
    id: number;
    price: number;
    discount: number;
    category:string;
    description:string;
    rating: number;
    stock:string;
    title:string;
    thumbnail:ImageData;
}





