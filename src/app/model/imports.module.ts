import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { SliderModule } from 'primeng/slider';
import { TagModule } from 'primeng/tag';
import { ProgressBarModule } from 'primeng/progressbar';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';
import { DialogModule } from 'primeng/dialog';
import { FileUploadModule } from 'primeng/fileupload';
import { PaginatorModule } from 'primeng/paginator';
import { AccordionModule } from 'primeng/accordion';
import { MenuModule } from 'primeng/menu';
import { CardModule } from 'primeng/card';
import { ReactiveFormsModule } from '@angular/forms';
import { InputNumberModule } from 'primeng/inputnumber';
import { BadgeModule } from 'primeng/badge';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';


@NgModule({
    declarations: [],
    imports: [
        ButtonModule,
        InputTextModule,
        CheckboxModule,
        RadioButtonModule,
        SplitButtonModule,
        TableModule,
        MultiSelectModule,
        DropdownModule,
        SliderModule,
        TagModule,
        ProgressBarModule,
        ToastModule,
        TooltipModule,
        DialogModule,
        FileUploadModule,
        PaginatorModule,
        AccordionModule,
        MenuModule,
        CardModule,
        ReactiveFormsModule,
        InputNumberModule,
        BadgeModule,
        CalendarModule,
        FormsModule,

    ],
    exports: [
        ButtonModule,
        FormsModule,
        InputTextModule,
        CheckboxModule,
        RadioButtonModule,
        SplitButtonModule,
        TableModule,
        MultiSelectModule,
        DropdownModule,
        SliderModule,
        TagModule,
        ProgressBarModule,
        ToastModule,
        TooltipModule,
        DialogModule,
        FileUploadModule,
        PaginatorModule,
        AccordionModule,
        MenuModule,
        CardModule,
        ReactiveFormsModule,
        InputNumberModule,
        BadgeModule,
        CalendarModule

    ]
})
export class ImportModule { }
