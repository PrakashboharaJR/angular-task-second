import { Component, OnInit } from '@angular/core';
import { Service } from 'src/app/service/service';

@Component({
    selector: 'cart',
    templateUrl: './cart.component.html',
})
export class CartComponent implements OnInit {

    //   Variable Declaration
    cart: any = [];
    subTotal: number = 0;
    taxPercent: number = 12; //I assign tax percent equals to 12% 
    taxAmount: number = 0;

    // Constructor
    constructor(
        private _service: Service
    ) {
        this._service.cartChanged$
            .subscribe(cart => {
                this.getCart();
            });
            
    }

    // Ng Onit
    ngOnInit(): void {
        this.getCart();
    }

    // Fetching Cart from localstorage using service.ts file
    getCart() {
        this.cart = this._service.getCart();
        this.subTotal = 0;
        this.taxAmount = 0;
        this.cart.forEach((product: any) => {
            this.subTotal = this.subTotal + product.price * product.count;
            this.taxAmount = this.taxAmount + this.taxPercent / 100 * product.price * product.count;
        });
    }

    // For changing product quantity
	updateCartQty(id: any, n: number) {
        this._service.updateCartQty(id, n);
	}

    // Deleting Product from cart
    remove(index: any) {
        this._service.remove(index);
    }


}