import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportModule } from '../model/imports.module';
import { CartRoutingModule } from './cart-routing.module';

import { CartComponent } from './cart/cart.component';

@NgModule({
    declarations: [
       CartComponent
    ],
    imports: [
        CommonModule,
        CartRoutingModule,
        ImportModule

      
    ]
})
export class CartModule { }
