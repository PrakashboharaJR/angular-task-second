import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportModule } from '../model/imports.module';
import { CheckoutRoutingModule } from './checkout-routing.module';

import { CheckoutComponent } from './checkout/checkout.component';

@NgModule({
    declarations: [
       CheckoutComponent
    ],
    imports: [
        CommonModule,
        CheckoutRoutingModule,
        ImportModule

      
    ]
})
export class CheckoutModule { }
