import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Service } from 'src/app/service/service';
import { Message } from 'primeng/api';

@Component({
    selector: 'checkout',
    templateUrl: './checkout.component.html',
})
export class CheckoutComponent implements OnInit {

    // Variable Declaration
    infoForm: FormGroup;
    paymentForm: FormGroup;
    activeIndex: number = 0;
    disablePayment: boolean = true;
    disableOrder: boolean = true;
    cart: any = [];
    subTotal: number = 0;
    taxPercent: number = 12; //I assign tax percent equals to 12% 
    taxAmount: number = 0;
    messages: Message[] = [];
    placeOrder: boolean = true;

    // Constructor
    constructor(
        private formBuilder: FormBuilder,
        private _service: Service
    ) {
        // InfoForm
        this.infoForm = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            phoneNumber: ['', [Validators.required, Validators.minLength(10)]],
            address: ['', Validators.required],
            locality: ['', Validators.required],
        });
        //Payment Form
        this.paymentForm = this.formBuilder.group({
            cardHolderName: ['', Validators.required],
            cardNumber: ['', Validators.required],
            expiryDate: ['', Validators.required],
            securityNumber: ['', Validators.required],

        });

        this._service.cartChanged$
            .subscribe(cart => {
                this.getCart();
            });
    }

    // Ng Onit
    ngOnInit(): void {
        this.getCart();
    }

    // Fetching Cart from localstorage using service.ts file
    getCart() {
        this.cart = this._service.getCart();
        this.subTotal = 0;
        this.taxAmount = 0;
        this.cart.forEach((product: any) => {
            this.subTotal = this.subTotal + product.price * product.count;
            this.taxAmount = this.taxAmount + this.taxPercent / 100 * product.price * product.count;
        });
    }

    // Deleting Product from cart
    remove(index: any) {
        this._service.remove(index);
    }

    // Next Button of Information Accordion
    onSubmitInfo() {
        if (this.infoForm.valid) {
            this.activeIndex = 1;
            this.disablePayment = false;
        }
    }

    // Next Button of Payment Accordion
    onSubmitPayment() {
        if (this.paymentForm.valid) {
            this.activeIndex = 2;
            this.disableOrder = false;
            this.placeOrder = false;
        }
    }

    // On Clicking place Order button
    onClickPlaceOrder() {
        if (this.cart != null && this.infoForm.valid && this.paymentForm.valid) {
            this.messages = [{ severity: 'success', detail: 'Order Placed', summary: 'Successfully' }];
            console.log(this.infoForm.value);
            console.log(this.paymentForm.value);
            console.log(this.cart);
            this.clearCheckoutPage();
        }
        // set timeout for clearing message pop up box automatically
        setTimeout(() => {
            this.messages = [];
        }, 2000);
    }

    // Clearing cart 
    clearCheckoutPage() {
        localStorage.removeItem('cart');
        this.cart = [];
        this._service.cartChanged.next(this.cart);
    }

}