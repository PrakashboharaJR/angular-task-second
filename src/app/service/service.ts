import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class Service {

    private url: string = 'https://dummyjson.com/products';
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    public cart: any[] = [];
    public cartChanged = new Subject<any>();
    public cartChanged$ = this.cartChanged.asObservable();


    // Constructor
    constructor(private http: HttpClient) { }


    // For Fetching Product List
    list(): Observable<any> {
        return this.http
            .get<any>(
                this.url, this.httpOptions
            )
    }

    // For Product Detail
    productDetail(id: any) {
        return this.http.get(this.url + '/' + id)
    }

    // Adding Product in Cart
    add(product: any, count: number) {
        const productFound = this.cart.find((element: any) => element.id === product.id);
        // If Item is found in cart update the count. 
        if (productFound) {
            productFound.count += count;
            if (productFound.count < 1) {
                let index = this.cart.map(function (e: any) { return e.id; }).indexOf(productFound.id);
                this.cart.splice(index, 1);
            }
        }
        // Else add the product in cart. 
        else {
            product.count = count;
            this.cart.push(product);
        }
        this.saveCart();
    }

    // Saving cart in localstorage
    saveCart() {
        localStorage.setItem('cart', JSON.stringify(this.cart));
        this.cartChanged.next(this.cart);
    }

    // Updating Qty
    updateCartQty(id: any, count: any) {
        let productFound = this.cart.find((element: any) => element.id === id);
        if (productFound) {
            if (productFound.count < 1) {
                productFound.count = 1;
            } else
                productFound.count += count;
            this.saveCart();
        }
    }

    // Deleting Product from cart
    remove(index: any) {
        this.cart.splice(index, 1);
        this.saveCart();
    }

    // Getting Cart from localstorage
    getCart() {
        let stringCart: any = localStorage.getItem('cart')
        let cart = JSON.parse(stringCart);
        return cart || [];
    }

    // Getting cart initially when app loads
    initCart() {
        let stringCart: any = localStorage.getItem('cart');
        let cart = JSON.parse(stringCart);
        if (cart) {
            this.cart = cart;
        }
    }

}
