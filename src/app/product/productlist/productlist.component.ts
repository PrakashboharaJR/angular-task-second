import { Component, OnInit } from '@angular/core';
import { Service } from 'src/app/service/service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {  Router } from '@angular/router';


@Component({
  selector: 'product',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']

})
export class ProductListComponent implements OnInit {

  // Variable Declaration
  searchForm!: FormGroup;
  loading: boolean = false;
  productList: any[] = [];
  filteredProductList: any[] = [];


  // Constructor
  constructor(
    private _service: Service,
    private formBuilder: FormBuilder,
    private route: Router
  ) {

    // Formgroup
    this.searchForm = this.formBuilder.group({
      searchTerm: ['']
    });

    // Acessing value on
    this.searchForm?.valueChanges.subscribe(value => {
      this.searchForProduct();
    });
  }


  // Ng Onit
  ngOnInit() {
    this.getList();
  }

  // Fetching Product List from API and Storing in Array
  getList() {
    this._service.list().subscribe((results) => {
      this.productList = results.products;
      this.filteredProductList = this.productList;
    });
  }

  // For filtering productlist by price and discount
  searchForProduct() {
    const searchTerm = this.searchForm.get('searchTerm')?.value?.toLowerCase();
    this.filteredProductList = this.productList.filter(item => item.title.toLowerCase().includes(searchTerm)
    );
  }
 
}