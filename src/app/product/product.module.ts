import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportModule } from '../model/imports.module';
import { ProductRoutingModule } from './product-routing.module';

import { ProductListComponent } from './productlist/productlist.component';
import { ProductDetailComponent } from './productdetail/productdetail.component';

@NgModule({
    declarations: [
        ProductListComponent,
        ProductDetailComponent
    ],
    imports: [
        CommonModule,
        ProductRoutingModule,
        ImportModule
      
    ]
})
export class ProductModule { }
