import { Component, OnInit } from '@angular/core';
import { Service } from 'src/app/service/service';
import { ActivatedRoute, Router } from '@angular/router';
import { Message } from 'primeng/api';

@Component({
  selector: 'product',
  templateUrl: './productdetail.component.html',

})
export class ProductDetailComponent implements OnInit {

  // Variable Declaration
  Id?: string | null;
  product: any;
  loading?: boolean;
  count: number = 1;
  messages: Message[] = [];

  // Constructor
  constructor(
    private route: ActivatedRoute,
    private _service: Service,
    private router: Router
  ) { }

  // Ng Onit
  ngOnInit(): void {
    this.route.paramMap
      .subscribe(params => {
        this.Id = params.get('id');
      });

    this.getDetails(this.Id);
  }

  // Getting Product Detail
  getDetails(id: any): void {
    this.loading = true;
    this._service.productDetail(id)
      .subscribe(product => {
        this.product = product;
        this.loading = false;
      });
  }

  // Adding product to cart
  addToCart(product: any) {
    this._service.add(product, this.count);
    this.messages = [{ severity: 'success', detail: 'Product added to cart', summary: 'Successfully' }];
    this.count = 1;
    // set timeout for clearing message pop up box automatically
    setTimeout(() => {
      this.messages = [];
    }, 1000);
  }

  // For changing product quantity
  changeCount(n: any) {
    this.count += n;
    if (this.count < 1) {
      this.count = 1;
    }
  }

}